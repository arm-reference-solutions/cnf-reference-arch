# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: Apache-2.0

VENV_DIR      ?= doc/venv
PYTHON        ?= python3

.PHONY: all
all:
	@echo "To figure out supplementing required subcommands"

.PHONY: venv
venv:
	@( \
	    if [ ! -d ${VENV_DIR} ]; then \
	      ${PYTHON} -m venv ${VENV_DIR}; \
	      . ${VENV_DIR}/bin/activate; \
	      ${PYTHON} -m pip install -r doc/requirements.txt; \
	    fi; \
	)

.PHONY: doc
doc: venv
	@( \
	    . ${VENV_DIR}/bin/activate; \
	    cd ./doc; \
	    make html; \
	)

.PHONY: doc-clean
doc-clean: venv
	@( \
	    . ${VENV_DIR}/bin/activate; \
	    cd ./doc; \
	    make clean; \
	)

.PHONY: doc-spellcheck
doc-spellcheck: venv
	@( \
		. ${VENV_DIR}/bin/activate; \
		cd ./doc; \
		SPHINXOPTS=-W make spelling; \
	)

.PHONY: help
help:
	@echo "Make Targets:"
	@echo " doc                  - Build the Sphinx documentation"
	@echo " doc-clean            - Remove the generated files from the Sphinx docs"
	@echo " doc-spellcheck       - Spellcheck the Sphinx documentation"
