#!/usr/bin/env bash

# Copyright (c) 2023, Arm Limited.
#
# SPDX-License-Identifier: Apache-2.0

set -ex

# Converts isolcpus's format of #-#,#,... to a list of CPU numbers that are isolated
list_isol_cpus() {
    local isocpus=$1
    #split isocpus on comma
    IFS=',' read -r -a array <<< "$isocpus"
    result=""
    for group in "${array[@]}"; do
        # if it is a range
        if [[ "$group" == *-* ]]; then
            # generate the range with spaces between each cpu in the range
            result+=$(seq "$(echo "$group" | cut -d '-' -f 1)" "$(echo "$group" | cut -d '-' -f 2)")
        else
            # just a single CPU
            result+=$group
        fi
        result+=' '
    done
    echo "$result"
}

echo "Running on Node: $NODE_NAME"
# Convert from comma separated list to space separated list
DPDK_PCIE_ADDRS=${PCIDEVICE_ARM_COM_DPDK//,/ } # populated by SR-IOV device plugin

# Read which CPUs we are binded to.
# Attempt a few different ways to be more portable
DPDK_CPUS=$(grep Cpus_allowed_list /proc/self/status | cut -f 2)
if [ -z "$DPDK_CPUS" ]; then
	DPDK_CPUS=$(cat /sys/fs/cgroup/"$(cat /proc/self/cpuset)"/cpuset.cpus)
fi
if [ -z "$DPDK_CPUS" ]; then
	DPDK_CPUS=$(cat /sys/fs/cgroup/cpuset/cpuset.cpus)
fi

# Translate whatever CPUs we are pinned to into Lcore IDs starting at 1. A single lcore is pinned to one isolated cpu.
lcore_cpus=$(list_isol_cpus "$DPDK_CPUS")
lcore_id=1
DPDK_LCORE_ARG=""
for cpu in $lcore_cpus; do
    DPDK_LCORE_ARG+="$lcore_id@$cpu,"
    lcore_id=$((lcore_id+1))
done
DPDK_LCORE_ARG="${DPDK_LCORE_ARG%,}" # remove trailing ','

# run dpdk-testpmd
DPDK_ARGS=$(for addr in $DPDK_PCIE_ADDRS; do echo -n " -a $addr "; done)
./build/app/dpdk-testpmd --lcores $DPDK_LCORE_ARG $DPDK_ARGS -- --forward-mode=5tswap --port-topology=loop --auto-start < <(sleep 10d)
# dpdk-testpmd exits when input is provided on stdin. Keep the application alive by having stdin sleep for 10 days.
set +x
