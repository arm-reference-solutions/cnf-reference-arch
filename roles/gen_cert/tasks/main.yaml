# Copyright (c) 2022-2024, Arm Limited.
#
# SPDX-License-Identifier: Apache-2.0
---
# Create a directory to hold certs unless user has already created one
- name: Validate cert_dir exists with proper permissions at {{ cert_dir }}
  ansible.builtin.file:
    path: "{{ cert_dir }}"
    state: directory
    mode: 0755
  register: cert_dir_obj

# Flow for creating the self signed certificate:
# 1) generate public/private keys
  # https://docs.ansible.com/ansible/latest/collections/community/crypto/openssl_privatekey_module.html#openssl-privatekey-generate-openssl-private-keys
# 2) Generate a Certificate Signing Request (CSR)
  # https://docs.ansible.com/ansible/latest/collections/community/crypto/openssl_csr_module.html#openssl-csr-generate-openssl-certificate-signing-request-csr
#   - Don't forget the SubjectAltName = DNS:domain-name
# 3) Use private key & CSR to generate a self signed certificate

- name: Generate OpenSSL private key
  community.crypto.openssl_privatekey:
    path: "{{ cert_dir_obj.path }}/domain.key"

- name: Generate subject alt name (SAN)
  ansible.builtin.set_fact:
    san: "DNS:{{ controller_fqdn }}"
  when: not (controller_fqdn | ansible.utils.ipaddr)

- name: Generate subject alt name (SAN) when controller_fqdn is in IP address type
  ansible.builtin.set_fact:
    san: "IP:{{ controller_fqdn }}"
  when: controller_fqdn | ansible.utils.ipaddr

- name: Generate Subject Alt Name (SAN) in list
  ansible.builtin.set_fact:
    sub_alt_name:
      - "{{ san }}"

- name: Append controller_ip_address to SAN if needed
  ansible.builtin.set_fact:
    sub_alt_name: "{{ sub_alt_name + ['IP:{{ controller_ip_address }}'] }}"
  when: controller_ip_address != controller_fqdn

- name: Generate Certificate Signing Request (CSR)
  community.crypto.openssl_csr_pipe:
    privatekey_path: "{{ cert_dir_obj.path }}/domain.key"
    common_name: "cnf-reference-arch-selfsigned"
    subject_alt_name: "{{ sub_alt_name }}"
    basic_constraints: "CA:TRUE"
  register: csr

- name: Create self-signed certificate from CSR
  community.crypto.x509_certificate:
    path: "{{ cert_dir_obj.path }}/domain.crt"
    csr_content: "{{ csr.csr }}"
    privatekey_path: "{{ cert_dir_obj.path }}/domain.key"
    provider: selfsigned
    return_content: true
  register: cert

- name: Copy certificate to local host
  ansible.builtin.fetch:
    src: "{{ cert_dir_obj.path }}/domain.crt"
    dest: ./domain.crt
    flat: true
