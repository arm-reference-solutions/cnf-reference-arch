# Copyright (c) 2023, Arm Limited.
#
# SPDX-License-Identifier: Apache-2.0
---
- name: Install packages via yum
  become: true
  ansible.builtin.yum:
    name: "{{ packages }}"
    state: present
  vars:
    packages:
      - python3
      - python3-pip # used to pip install later packages
      - tar # used to install CNIs & K8s binaries
      - cri-tools-1.25.0 # used by kubeadm.
      - yum-versionlock # used later to lock runc/containerd versions
      - git # used to fetch igb_uio sources
      - conntrack-tools # required by kubeadm
      - iptables-nft
      - socat
      - iproute-tc
      - "kernel-devel == {{ ansible_kernel.rsplit('.', 1)[0] }}"
      - "kernel-headers == {{ ansible_kernel.rsplit('.', 1)[0] }}"
        # rsplit(...)[0] removes the last substring starting with '.'
        # transforms the ansible_kernel from <kernel_version>.aarch64 to
        # just <kernel_version> so yum installs the right package version.
        # kernel-devel and kernel-headers are used to compile igb_uio

- name: Ensure incompatible packages are removed
  become: true
  ansible.builtin.yum:
    name: "{{ packages }}"
    state: absent
  vars:
    packages:
      - ec2-net-utils

- name: Install Docker
  become: true
  ansible.builtin.command: amazon-linux-extras install docker=stable -y
  changed_when: true

- name: Install runc and containerd
  become: true
  ansible.builtin.yum:
    name: "{{ packages }}"
    state: present
  vars:
    packages:
      # values usually mirror EKS image builder
      # https://github.com/awslabs/amazon-eks-ami/blob/master/eks-worker-al2-variables.json
      - runc-1.1.5-1.amzn2
      - containerd-1.6.*

- name: Lock runc and containerd versions
  become: true
  community.general.yum_versionlock:
    state: present
    name: "{{ packages }}"
  vars:
    packages:
      - runc-1.1.5-1.amzn2
      - containerd-1.6.*

- name: Make CNI and Kubernetes binary directories
  ansible.builtin.file:
    path: "{{ item }}"
    state: directory
    mode: 0755
  become: true
  loop:
    - /opt/cni/bin
    - /usr/bin
    - /etc/systemd/system/kubelet.service.d

- name: Download & extract CNI plugins
  ansible.builtin.unarchive:
    src: https://github.com/containernetworking/plugins/releases/download/v1.2.0/cni-plugins-linux-arm64-v1.2.0.tgz
    dest: /opt/cni/bin
    remote_src: true
  become: true

- name: Download Kubernetes binaries
  ansible.builtin.get_url:
    url: "https://dl.k8s.io/release/v1.25.0/bin/linux/arm64/{{ item }}"
    dest: "/usr/bin/{{ item }}"
    mode: 0755
  become: true
  loop:
    - kubeadm
    - kubelet
    - kubectl

- name: Download Kubernetes systemd service files
  ansible.builtin.get_url:
    url: "{{ item.url }}"
    dest: "{{ item.dest }}"
    mode: 0640
  loop:
    - url: https://raw.githubusercontent.com/kubernetes/release/v0.15.1/cmd/kubepkg/templates/latest/rpm/kubelet/kubelet.service
      dest: /etc/systemd/system/kubelet.service
    - url: https://raw.githubusercontent.com/kubernetes/release/v0.15.1/cmd/kubepkg/templates/latest/rpm/kubeadm/10-kubeadm.conf
      dest: /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
  become: true

- name: Modify iptables default forwarding rule using kubelet service
  become: true
  ansible.builtin.lineinfile:
    line: ExecStartPre=/sbin/iptables -P FORWARD ACCEPT -w 5
    regexp: '^ExecStartPre='
    insertbefore: '^ExecStart='
    path: /etc/systemd/system/kubelet.service
# see https://github.com/aws/amazon-vpc-cni-k8s/blob/v1.15.0/docs/troubleshooting.md#tip-running-cni-on-non-eks-ami

- name: Enable kubelet systemd service
  ansible.builtin.systemd:
    name: kubelet.service
    enabled: true
    state: started
  become: true
