# Copyright (c) 2023-2024, Arm Limited.
#
# SPDX-License-Identifier: Apache-2.0
---
- name: Warn user if dpdk_driver is not expected
  ansible.builtin.assert:
    that:
      - dpdk_driver in driver_list
    fail_msg: "'dpdk_driver={{ dpdk_driver }}' is not expected. Did you input it correctly?"
  vars:
    driver_list:
      - vfio-pci
      - mlx5_core
      - igb_uio
  ignore_errors: true # noqa ignore-errors want task text to be red and alerting to end user

- name: Install dpdk-devbind.py script to bind proper drivers
  ansible.builtin.get_url:
    url: https://raw.githubusercontent.com/DPDK/dpdk/v23.03/usertools/dpdk-devbind.py
    dest: /usr/local/bin/dpdk-devbind.py
    mode: 0755
  become: true

- name: Bind user-provided PCIe addresses to correct Linux driver
  ansible.builtin.command: "/usr/local/bin/dpdk-devbind.py -b {{ dpdk_driver }} {{ pcie_addr | list | join(' ') }} "
  become: true
  when: pcie_addr_type == "VF" or not (deploy_on_vfs | bool)
  register: devbind_output

- name: Fail if any PCIe address has active routes
  ansible.builtin.fail:
    msg: "At least one PCIe address has an active route. Binding this to {{ dpdk_driver }} may cause a loss in connectivity."
  when:
    - pcie_addr_type == "VF" or not (deploy_on_vfs | bool)
    - '"Not modifying" in devbind_output.stderr'

- name: Handle each user-provided address
  ansible.builtin.include_tasks: bind-one-vf.yaml
  loop: "{{ pcie_addr }}"
  loop_control:
    loop_var: addr
  when: pcie_addr_type == "PF" and (deploy_on_vfs | bool)
