# Copyright (c) 2022-2024, Arm Limited.
#
# SPDX-License-Identifier: Apache-2.0
---
- name: Initialize kubeadm parameters
  ansible.builtin.set_fact:
    kubeadm_init_params: "--cri-socket unix:///run/containerd/containerd.sock {{ kubeadm_init_extra_args }}"

- name: Set Pod CIDR
  ansible.builtin.set_fact:
    kubeadm_init_params: "{{ kubeadm_init_params }} --pod-network-cidr={{ pod_cidr }}"
  when: not aws_inst

- name: Initialize Kubernetes cluster with kubeadm
  # We use the containerd container engine here. Since we installed docker, we have to specify the cri socket explicitly
  # You can also add a task for a dockershim install and remove the cri socket parameter
  ansible.builtin.command: 'kubeadm init {{ kubeadm_init_params }}'
  become: true
  changed_when: true

- name: Make kube directory
  ansible.builtin.file:
    path: "/home/{{ ansible_user_id }}/.kube"
    state: directory
    mode: 0755

- name: Copy kube config from /etc/kubernetes/admin.conf to ~/.kube/config
  ansible.builtin.copy:
    src: /etc/kubernetes/admin.conf
    dest: "/home/{{ ansible_user_id }}/.kube/config"
    remote_src: true
    group: "{{ ansible_real_group_id }}"
    owner: "{{ ansible_real_user_id }}"
    mode: 0644
  become: true

- name: Generate join command
  ansible.builtin.command: kubeadm token create --print-join-command
  changed_when: false
  register: join_command

- name: Copy join command to local machine
  ansible.builtin.copy:
    content: "{{ join_command.stdout_lines[0] }} --cri-socket unix:///run/containerd/containerd.sock"
    dest: "./join_command.txt"
    mode: 0664
  delegate_to: localhost
  run_once: true

- name: Copy /etc/kubernetes/admin.conf to local machine for later transfer to worker machines
  ansible.builtin.fetch:
    src: /etc/kubernetes/admin.conf
    dest: admin.conf
    flat: true
  become: true

- name: Start Docker registry
  community.docker.docker_container:
    name: registry
    image: registry:2
    state: started
    detach: true
    restart_policy: always # restart on crash or when docker daemon restarts
    volumes:
      - "{{ cert_dir_obj.path }}:/certs"
    env:
      REGISTRY_HTTP_ADDR: 0.0.0.0:443
      REGISTRY_HTTP_TLS_CERTIFICATE: /certs/domain.crt
      REGISTRY_HTTP_TLS_KEY: /certs/domain.key
    published_ports:
      - 443:443

- name: Remove taint on controller nodes
  ansible.builtin.command: kubectl taint nodes --all node-role.kubernetes.io/control-plane-
  run_once: true
  when: remove_taint | bool
