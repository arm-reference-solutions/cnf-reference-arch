#!/usr/bin/env bash

# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: Apache-2.0

set -eo pipefail
grep -q "isolcpus" /proc/cmdline || { echo "no isolcpus in /proc/cmdline"; exit 1; }
grep Cpus_allowed_list /proc/self/status | cut -f 2
# With pipefail, script will error if Cpus_allowed_list is not present in /proc/self/status.
# It should be present with any kernel post 4.19, so no need for robust error handling.
