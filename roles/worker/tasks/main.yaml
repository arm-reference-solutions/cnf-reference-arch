# Copyright (c) 2022-2023, Arm Limited.
#
# SPDX-License-Identifier: Apache-2.0
---
- name: Get list of non-isolated CPUs
  ansible.builtin.script: list-nonisolcpus.sh
  register: nonisolcpus_script

- name: Force 4 CPU machines to share 1 CPU between pods and system daemons
  ansible.builtin.command: echo 0
  register: nonisolcpus_shared
  when:
    - ansible_processor_count == 4

- name: Set nonisolated CPUs
  ansible.builtin.set_fact:
    nonisolcpus: "{{ 0 if ansible_processor_count == 4 else nonisolcpus_script.stdout_lines[0] }}"

# Uncomment this below task if the list-nonisolcpus.sh script does
# not fit your needs. Also comment out the above command
#
# - name: Get reserved_cpus from per-host variables
#   command: echo "{{ reserved_cpus }}"
#   register: nonisolcpus

- name: Validate 1G hugepages are present
  ansible.builtin.command: awk '{ if ($1 == 0) exit 1 }' /sys/kernel/mm/hugepages/hugepages-1048576kB/nr_hugepages
  changed_when: false
  failed_when: false # fail with custom error message on next task
  register: hugepages

- name: Fail if no 1G hugepages
  ansible.builtin.fail:
    msg: "Please reserve at least one 1G hugepage!"
  when: hugepages.rc != 0

- name: Print kubelet cpus
  ansible.builtin.debug:
    msg: "Using these non isolated CPUs for all non-pod workloads: {{ nonisolcpus }}"

- name: Create tmp dir
  ansible.builtin.tempfile:
    state: directory
  register: tmpdir

- name: Copy join command to remote machine
  ansible.builtin.copy:
    src: join_command.txt
    dest: "{{ tmpdir.path }}/join_command.sh"
    mode: 0777
  when: not skip_join_cluster | bool

- name: Execute join command on remote machine
  ansible.builtin.command: sh "{{ tmpdir.path }}"/join_command.sh
  become: true
  when: not skip_join_cluster | bool

- name: Stop kubelet service
  ansible.builtin.service:
    name: kubelet
    state: stopped
  become: true

- name: Add static CPU policy to kubelet config
  ansible.builtin.lineinfile:
    insertafter: EOF
    path: /var/lib/kubelet/config.yaml
    line: "cpuManagerPolicy: static"
  become: true

- name: Remove old CPU policy state
  ansible.builtin.file:
    path: /var/lib/kubelet/cpu_manager_state
    state: absent
  become: true

- name: Add non-isolcpus to be reserved for kubelet/system
  ansible.builtin.lineinfile:
    insertafter: EOF
    path: /var/lib/kubelet/config.yaml
    line: 'reservedSystemCPUs: "{{ nonisolcpus }}"'
  become: true

- name: Set maxPods based on AWS VPC CNI limits
  ansible.builtin.include_tasks:
    file: max-pods.yaml
  when: aws_inst

- name: Restart kubelet
  ansible.builtin.service:
    name: kubelet
    state: started
  become: true

- name: Create ~/.kube
  ansible.builtin.file:
    path: "/home/{{ ansible_user_id }}/.kube"
    state: directory
    mode: 0755

- name: Copy admin.conf to remote machine
  ansible.builtin.copy:
    src: admin.conf
    dest: "/home/{{ ansible_user_id }}/.kube/config"
    owner: "{{ ansible_real_user_id }}"
    group: "{{ ansible_real_group_id }}"
    mode: 0644

- name: Wait for all nodes to be ready
  ansible.builtin.command: kubectl wait --for=condition=Ready nodes --all --timeout="{{ node_wait_timeout }}"
  changed_when: false
  run_once: true

- name: Clone SR-IOV CNI onto one worker
  ansible.builtin.git:
    repo: https://github.com/k8snetworkplumbingwg/sriov-cni
    dest: "{{ tmpdir.path }}/sriov-cni"
    version: v2.6.3
  run_once: true
  when:
    - not (ali_cloud_inst | bool)
    - not (aws_inst | bool)

- name: Use controller's fqdn as docker_server
  ansible.builtin.set_fact:
    docker_server: "{{ controller_fqdn }}"
  when:
    - not (ali_cloud_inst | bool)
    - not (aws_inst | bool)

- name: Print variables of docker server
  ansible.builtin.debug:
    msg:
      - "docker_server: {{ docker_server }}"
  when:
    - not (ali_cloud_inst | bool)
    - not (aws_inst | bool)

- name: Build and push SR-IOV CNI image for arm
  run_once: true
  community.docker.docker_image:
    build:
      path: "{{ tmpdir.path }}/sriov-cni"
    name: "{{ docker_server }}:443/sriov-cni"
    tag: v2.6.3
    push: true
    source: build
  when:
    - not (ali_cloud_inst | bool)
    - not (aws_inst | bool)

- name: Change amd64 to arm64 in SR-IOV CNI example deployment # change all amd64 to arm64
  run_once: true
  ansible.builtin.replace:
    path: "{{ tmpdir.path }}/sriov-cni/images/k8s-v1.16/sriov-cni-daemonset.yaml"
    regexp: "amd"
    replace: "arm"
  when:
    - not (ali_cloud_inst | bool)
    - not (aws_inst | bool)

- name: Change SR-IOV CNI deployment image to our image
  run_once: true
  ansible.builtin.replace:
    path: "{{ tmpdir.path }}/sriov-cni/images/k8s-v1.16/sriov-cni-daemonset.yaml"
    regexp: "image: .*"
    replace: "image: {{ docker_server }}:443/sriov-cni:v2.6.3"
  when:
    - not (ali_cloud_inst | bool)
    - not (aws_inst | bool)

- name: Apply SR-IOV CNI daemonset
  ansible.builtin.command: kubectl apply -f "{{ tmpdir.path }}/sriov-cni/images/k8s-v1.16/sriov-cni-daemonset.yaml"
  run_once: true
  changed_when: true
  when:
    - not (ali_cloud_inst | bool)
    - not (aws_inst | bool)

- name: Wait for SR-IOV CNI pods to become created
  ansible.builtin.command: "kubectl get po --namespace=kube-system --selector name=sriov-cni --output=jsonpath='{.items[*].metadata.name}'"
  register: sriov_cni_pods_created
  until: item in sriov_cni_pods_created.stdout
  retries: 5
  delay: 3
  changed_when: false
  with_items:
    - sriov-cni
  when:
    - not (ali_cloud_inst | bool)
    - not (aws_inst | bool)

- name: Wait for SR-IOV CNI pods to be ready
  ansible.builtin.command: kubectl wait --namespace=kube-system --for=condition=Ready pods --selector name=sriov-cni --timeout=60s
  retries: 5
  run_once: true
  changed_when: false
  when:
    - not (ali_cloud_inst | bool)
    - not (aws_inst | bool)

- name: Install SR-IOV Device Plugin
  # despite the deployment coming from v3.4.0, it still uses the :latest tag. We may want to change this depending if they have arm64 CI/CD.
  ansible.builtin.command: >
    kubectl apply -f https://raw.githubusercontent.com/k8snetworkplumbingwg/sriov-network-device-plugin/v3.4.0/deployments/k8s-v1.16/sriovdp-daemonset.yaml
  run_once: true
  changed_when: true
