..
  # Copyright (c) 2022,2024, Arm Limited.
  #
  # SPDX-License-Identifier: Apache-2.0

.. cnf-reference-arch documentation master file, created by
   sphinx-quickstart on Mon Apr 18 14:40:41 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

###############################################
Networking Solution: CNF Reference Architecture
###############################################

********
Contents
********

.. toctree::
   :maxdepth: 2

   overview
   quickstart
   user_guide/index
   troubleshooting
   design/index
   license_file_link
   changelog
   faq
