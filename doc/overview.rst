..
  # Copyright (c) 2022-2024, Arm Limited.
  #
  # SPDX-License-Identifier: Apache-2.0

########
Overview
########

************
Introduction
************

This software provides a management/orchestration template for
containerized networking applications on various Arm platforms, by
leveraging cloud-native and Kubernetes cluster technologies. The software
is implemented and validated on AArch64.

*********************
Solution Architecture
*********************

.. figure:: images/k8s-cluster.png
   :align: center

   CNF Reference Architecture solution architecture

The solution is prepared to setup a single or multi-node Kubernetes (K8s) cluster. The cluster is created by executing an Ansible playbook on a separate management node. In the case of the single-node cluster, the worker nodes and controller node are running on the same physical machine. Each node in the K8s cluster utilizes the ``containerd`` container runtime. The worker nodes have hugepages and isolated CPUs dedicated for the networking application. These CPUs and hugepages are managed and assigned to the K8s pods by the K8s control plane. The controller node also runs a private Docker registry, which stores custom-built container images.

*********
Use Cases
*********

The solution's goal is to establish a K8s cluster template
to run networking applications. Sample application(s) are included as a reference.

The implementation mechanism of each use case below will be described
in detail in later chapter of this documentation.

* **K8s Cluster Template**

  Setup of K8s cluster template to run cloud-native networking applications.

* **DPDK Testpmd**

  Deployment of cloud-native Data Plane Development Kit (DPDK) based Testpmd application.

********
Features
********

* Automated Kubernetes cluster setup
* Automated private Docker registry setup
* Automated pinning of networking workload to isolated CPU cores

Implementation Considerations
-----------------------------

There are several technical considerations
in the implementation process:

* **Template for creating a Kubernetes cluster for any networking application**

  End users can take this software to instantiate any
  cloud native network functions, e.g., Snort3 & VPP based
  security router per their actual use case.

* **Provide network scalability and high availability**

  The system or application should be easily scalable to handle bigger
  amounts of work, or to be easily expanded, in response to increased demand
  for network processing capability.

* **Enable centralized and distributed deployment**

  The control plane controllers and data plane workers could run on the same
  node or different nodes.

* **Support applications from edge to cloud – K3s, Micro-K8s, K8s**

  Cloud computing has led many organizations to centralize their services
  within large data centers. However, new end-user experiences like the
  Internet of Things (IoT) require service provisioning closer to the outer
  "edges" of a network, where the physical devices exist.
  In the future, this solution will target more container orchestrators than just K8s.

Tested Platforms
~~~~~~~~~~~~~~~~

The below machines have been validated as Kubernetes cluster nodes:

  * Ampere Altra (Neoverse-N1) running Ubuntu 20.04
  * AWS EC2 c6gn.xlarge, c6gn.2xlarge and c7gn instance types running Amazon Linux 2

The below operating systems have been validated as management nodes:

  * Ubuntu 20.04

********************
Repository Structure
********************

The code repository are stored in Arm GitLab arm-reference-solutions group,
with below link addresses.

* https://gitlab.arm.com/arm-reference-solutions/cnf-reference-arch

Under the ``cnf-reference-arch`` repository are the following directories/files:
::

      ├── CHANGELOG.rst           - Change notes for each releases
      ├── create-cluster.yaml     - Ansible playbook to setup K8s cluster
      ├── doc/                    - Sphinx based documents
      ├── examples/               - Example networking applications to deploy
      ├── inventory.ini           - Inventory file listing the K8s cluster nodes
      ├── LICENSE.rst             - License file of this project
      ├── MAINTAINERS             - Maintainer recording of specific module/use-cases
      ├── Makefile                - Top level makefile
      ├── README.md               - Project introduction file
      ├── roles/                  - Ansible roles consumed by create-cluster.yaml
      ├── tools/                  - Tools used to manage the project

******************
Repository License
******************

The software is provided under an Apache 2.0 license (more details in
`Apache 2.0 <https://www.apache.org/licenses/LICENSE-2.0>`_
).

Contributions to the project should follow the same license.

*****************************
Contributions and Bug Reports
*****************************

This project has not put in place a process for contributions currently.

For bug reports, please submit an Issue via GitLab.

********************
Feedback and Support
********************

To request support please contact Arm at support@arm.com. Arm licensees may
also contact Arm via their partner managers.

*************
Maintainer(s)
*************

- Nathan Brown <Nathan.Brown@arm.com>
- Dhruv Tripathi <Dhruv.Tripathi@arm.com>
