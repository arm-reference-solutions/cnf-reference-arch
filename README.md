# CNF Reference Architecture

Copyright (c) 2022,2024, Arm Limited. All rights reserved.

## Introduction

Cloud-native Network Function (CNF) Reference Architecture (CRA) provides a management/orchestration template for containerized networking applications. It is implemented, validated, and deployed on Arm AArch64 architecture.

CRA solution architecture is shown in the following image:
![CRA solution architecture](doc/images/k8s-cluster.png "CRA solution architecture")

CRA serves multiple purposes,
* Showcase technologies useful to deploy a cloud-native networking application
* Enable deploying custom networking applications
* Provide users with an out-of-the-box experience for rapid development

CRA has been validated against the following platforms:
* Ampere Altra (Neoverse-N1) as cluster node(s)
* Ubuntu 20.04 machine as management node


## Important Notes

### Safety and Security

Arm Neoverse reference design software solutions are example software projects containing downstream versions of open source components. Although the components in these solutions track their upstream versions, users of these solutions are responsible for ensuring that, if necessary, these components are updated before use to ensure they contain any new functional or security fixes that may be required

# Getting Started

Please refer to the [Quickstart Guide](./doc/quickstart.rst) to get started using this solution. The Quickstart Guide and complete documentation generated from this repository are also available in the [CNF Reference Architecture online documentation](https://cnf-reference-arch.docs.arm.com).

# Limitations and Known Issues

* Users of this software stack must consider safety and security implications according to their own usage goals.
* Does not provide native traffic generator.
* Application pods using Mellanox VFs require the privileged security context.

## Feedback and Support
Arm welcomes any feedback. To provide feedback or to request support, please contact project maintainer by email Nathan Brown <nathan.brown@arm.com>, or check 'MAINTAINERS' file in top level directory of repository for module/feature/use-case specific maintainers.

Arm licensees may also contact Arm via their partner managers to request support.
