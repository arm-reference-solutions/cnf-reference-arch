..
  # Copyright (c) 2022,2024, Arm Limited.
  #
  # SPDX-License-Identifier: Apache-2.0

#########################
Changelog & Release Notes
#########################

*********************************
CNF Reference Architecture v24.03
*********************************

New Features
============

  * Added AWS support for the entire application
  * Added PF deployment support
  * Added high availability on single node and multi-node cluster

Changed
=======

  * Application support is moved from L3 forward sample application to testpmd sample application
  * Tested platforms: AWS EC2 Instance

    * DUT

      * c6gn.2xlarge instance

        * Amazon Linux 2
        * Kernel ``5.10.184-175.731.amzn2.aarch64``
        * Security group settings

            * Security group settings added same as this `Kubernetes Ports and Protocols guidelines <https://v1-25.docs.kubernetes.io/docs/reference/networking/ports-and-protocols/>`_
            * Additionally for allowing traffic between x86 and arm EC2 give permissions to all ports and protocol in inbound rules.

        * Secondary ENI attached at device index 1, with ``node.k8s.amazonaws.com/no_manage`` set to ``true``

    * Management Node

      * Ubuntu 20.04 system

        * Python 3.8
        * Ansible 6.5.0


  * Main software components versions:

    * Ansible 6.5.0
    * DPDK v22.11

Limitations
===========

  * Arm Neoverse reference design software solutions are example software projects containing downstream versions of open source components. Although the components in these solutions track their upstream versions, users of these solutions are responsible for ensuring that, if necessary, these components are updated before use to ensure they contain any new functional or security fixes that may be required
  * Does not provide native traffic generator.
  * Application pods using Mellanox VFs require the privileged security context.
  * High availability case does not support automatic traffic re-routing when the node is destroyed and recreated.
  * Dataplane interface needs to be on same NUMA node as isolated CPUs and available hugepages.

Resolved Issues
===============

None.

Known Issues
============

None.

*********************************
CNF Reference Architecture v22.12
*********************************

New Features
============

  * Automated Kubernetes cluster setup
  * Automated private Docker registry setup
  * Automated pinning of networking workload to isolated CPU cores

Changed
=======

As the initial version, the features in this release are summarized as
below:

Tested platforms:

  * DUT

    * Ampere Altra (Neoverse-N1)

      * Ubuntu 20.04.3 LTS (Focal Fossa)
      * Kernel 5.17.0-051700-generic

    * Container Image

      * Latest Ubuntu: 22.04 (as of this release)

  * NIC

    * Mellanox ConnectX-5

      * OFED driver: MLNX_OFED_LINUX-5.4-3.1.0.0
      * Firmware Version 16.30.1004 (MT_0000000013)

    * Intel X710

      * Firmware version: 6.01

  * Management Node

    * Ubuntu 20.04 system

      * Python 3.8
      * Ansible 6.5.0

Main software components versions:

  * Ansible 6.5.0
  * DPDK v21.11

Documentation:

  * Initial documentation with README, overview, quickstart guide, user
    guide, solution design and FAQ.

Tools:

  * Added scripts for:

    * Sphinx based documentation generation
    * Document spelling check
    * Commit log format check
    * License-Identifier check

Limitations
===========

  * Users of this software stack must consider safety and security implications according to their own usage goals.
  * Does not provide native traffic generator.
  * Application pods using Mellanox VFs require the privileged security context.
  * DPDK L3 forward sample application deployed in K8s cluster only supports single Ethernet port case.

Resolved Issues
===============

None.

Known Issues
============

None.

